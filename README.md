# Fast Accounting 

This is a pipeline to develop classification model on server. The model in trained on [Fashion-MNIST dataset](https://github.com/zalandoresearch/fashion-mnist#why-we-made-fashion-mnist) by PyTorch. Model of resnet_lite archived 92.2% in accurcy. Model of resnet18 archived 95.1% in accurcy. An API of Flask is also delivered which returns top 3 proposal results and their score. 


## Installation 
You can use [Docker](https://docs.docker.com/engine/install/ubuntu/) or [Anaconda](https://docs.anaconda.com/anaconda/install/). Then install libaraies list in requirements by 

```python
    pip3 install --upgrade pip
    sudo apt install python3-testresources
    pip3 install -r requirements.txt
```
## Train Model

```python
    python3 train.py -net resnet_lite -gpu 
```
### Support Nets List
|Nets|    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|0|mobilenet  |shufflenet  |vgg11|densenet121|googlenet|inceptionv3      |resnet18 |preactresnet18 |resnext50  |attention56|seresnet18 |nasnet|wideresnet|stochasticdepth18 |squeezenet|
|1|mobilenetv2|shufflenetv2|vgg13|densenet161|         |inceptionv4      |resnet34 |preactresnet34 |resnext101 |attention92|seresnet34 |      |          |stochasticdepth34 |          |
|2|           |            |vgg16|densenet201|         |inceptionresnetv2|resnet50 |preactresnet50 |resnext152 |           |seresnet50 |      |          |stochasticdepth50 |          |
|3|           |            |vgg19|           |         |xception         |resnet101|preactresnet101|           |           |seresnet101|      |          |stochasticdepth101|          |
|4|           |            |     |           |         |                 |resnet152|preactresnet152|           |           |seresnet152|      |          |                  |          |



## Test Model
```python
    python3 test.py -gpu -weight ./checkpoint/resnet_lite/Saturday_03_April_2021_19h_39m_04s/resnet_lite-200-regular.pth -net resnet_lite
```

## Flask
Set local server.
```python
    python3 app.y
```

Sent request.
```python
    resp = requests.post("http://localhost:5000/predict",
                     files={"file": open('./data/FashionMNIST/test_images/1001.bmp','rb')})

    print(resp.json())
```
Extracted [Fashion-MNIST dataset](https://github.com/zalandoresearch/fashion-mnist#why-we-made-fashion-mnist) test images in the folder below:
```
|---data 
    |---FashionMNIST 
        |---test_images
```
