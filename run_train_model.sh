python3 train.py -net resnet_lite -gpu

# python train.py -net squeezenet
# python train.py -net mobilenet
# python train.py -net mobilenetv2
# ## python train.py -net shufflenet
# ## python train.py -net shufflenetv2
# python train.py -net vgg11
# python train.py -net vgg13
# python train.py -net vgg16
# python train.py -net vgg19
# python train.py -net densenet121
# python train.py -net densenet161
# python train.py -net densenet201
# python train.py -net googlenet
# python train.py -net inceptionv3
# python train.py -net inceptionv4
# python train.py -net inceptionresnetv2
# python train.py -net xception
# python train.py -net resnet18
# python train.py -net resnet34
# python train.py -net resnet50
# python train.py -net resnet101
# python train.py -net resnet152
# python train.py -net preactresnet18
# python train.py -net preactresnet34
# python train.py -net preactresnet50
# python train.py -net preactresnet101
# python train.py -net preactresnet152
# python train.py -net resnext50
# python train.py -net resnext101
# python train.py -net resnext152
# python train.py -net attention56
# python train.py -net attention92
# python train.py -net seresnet18
# python train.py -net seresnet34
# python train.py -net seresnet50
# python train.py -net seresnet101
# python train.py -net seresnet152
# ## python train.py -net nasnet
# python train.py -net wideresnet
# python train.py -net stochasticdepth18
# python train.py -net stochasticdepth34
# python train.py -net stochasticdepth50
# python train.py -net stochasticdepth101