#app.py
#!/usr/bin/env python3

""" 
API in Flask.
Gives top3 prediction from model.
"""
import io
from PIL import Image
import torch
import torchvision.transforms as transforms
from utils import get_network
from flask import Flask, jsonify, request

from models.resnet_lite import resnet_lite

app = Flask(__name__)
class_name = {0:'T-shirt',1:'Trouser',2:'Pullover',3:'Dress',4:'Coat',5:'Sandal',6:'Shirt',7:'Sneaker',8:'Bag',9:'Ankle boot'}

net = resnet_lite()
net.load_state_dict(torch.load('./checkpoint/resnet_lite/Saturday_03_April_2021_19h_39m_04s/resnet_lite-200-regular.pth'))
net.eval()

def transform_image(image_bytes):
    my_transforms = transforms.Compose([transforms.ToTensor(),
                                        transforms.Normalize(mean=0.5,std=0.25)])
    image = Image.open(io.BytesIO(image_bytes))
    return my_transforms(image).unsqueeze(0)

def get_prediction(image_bytes):
    tensor = transform_image(image_bytes=image_bytes)
    outputs = net(tensor)
    _, ids = outputs.topk(3, 1, largest=True, sorted=True)

    ids = ids[0].numpy().tolist()
    scores = outputs[0].data.numpy().tolist()

    results = {}
    for p in ids:
        score = '{:.4f}'.format(scores[p])
        results[class_name[p]] = score
    
    return results

@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        file = request.files['file']
        img_bytes = file.read()
        results = get_prediction(image_bytes=img_bytes)

        return jsonify(results)

if __name__ == '__main__':
    app.run()
